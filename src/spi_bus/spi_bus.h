#ifndef EYMDEV__SPI_COMM_BUS__H
#define EYMDEV__SPI_COMM_BUS__H

#include "comm_bus_interface.h"

#define SPI_SPEED_4MHZ_CPU      2'000'000UL
#define SPI_SPEED_8MHZ_CPU      4'000'000UL
#define SPI_SPEED_16MHZ_CPU     8'000'000UL
#define SPI_SPEED_32MHZ_CPU     16'000'000UL

namespace eymonttdev {

    using GPIO_Pin = uint8_t;
    typedef void (*cb_cs_control_fn)(bool level);

    enum class SPI_Mode_t
    {
        MODE_0,     // CPOL = 0, CPHA = 0
        MODE_1,     //        0         1
        MODE_2,     //        1         0
        MODE_3      //        1         1
    };

    enum class SPI_Byte_Order_t { MSBYTE, LSBYTE };
    enum class SPI_Bit_Order_t  { MSBIT, LSBIT };

    struct SPI_Settings
    {
        const unsigned long speed;
        const SPI_Mode_t mode;
        const SPI_Byte_Order_t byte_order;
        const SPI_Bit_Order_t bit_order;
        const GPIO_Pin MISO;
        const GPIO_Pin MOSI;
        const GPIO_Pin SCK;
        const GPIO_Pin chipSelect;
        const bool dedicated;
    };

    class SPIBus : public PeripheralCommBus
    {
        public:
            inline SPIBus(cb_delay_fn fn_delay_ptr, cb_cs_control_fn fn_cs_control_ptr)
                : PeripheralCommBus(fn_delay_ptr)
                , cs_control_fn_(fn_cs_control_ptr)
                , mode_(SPI_Mode_t::MODE_1)
                , byte_order_(SPI_Byte_Order_t::MSBYTE)
                , bit_order_(SPI_Bit_Order_t::LSBIT)
                , dedicated_{false}
            {};
            inline COMM_BUS_RETURN_TYPE init(SPI_Settings settings)
            {
                clock_speed_ = settings.speed;
                mode_ = settings.mode;
                byte_order_ = settings.byte_order;
                bit_order_ = settings.bit_order;
                MISO_ = settings.MISO;
                MOSI_ = settings.MOSI;
                SCK_ = settings.SCK;
                CS_ = settings.chipSelect;
                dedicated_ = settings.dedicated;
                return COMM_SUCCESS;
            }
            inline GPIO_Pin MISO() { return MISO_; }
            inline GPIO_Pin MOSI() { return MOSI_; }
            inline GPIO_Pin SCK()  { return SCK_; }
            inline GPIO_Pin CS()   { return CS_; }

        protected:
            cb_delay_fn delay_fn_;
            cb_cs_control_fn cs_control_fn_;

            unsigned long clock_speed_{SPI_SPEED_16MHZ_CPU};
            SPI_Mode_t mode_;
            SPI_Byte_Order_t byte_order_;
            SPI_Bit_Order_t bit_order_;
            GPIO_Pin MISO_{255};
            GPIO_Pin MOSI_{255};
            GPIO_Pin SCK_{255};
            GPIO_Pin CS_{255};
            bool dedicated_;
    };

} // namespace eymonttdev

#endif  /* EYMDEV__SPI_COMM_BUS__H */