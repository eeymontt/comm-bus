#include "spi_bus_teensy41.h"

namespace eymonttdev {

    COMM_BUS_RETURN_TYPE SPITeensy41Bus::write_register(
        unsigned char dev_addr, unsigned char reg_addr, const unsigned char *reg_data, unsigned char cnt)
    {
        if (!dedicated_)
        {
            // set the speed to expected
            ;
        }

        /* @to-do @eeymontt implement */

        return COMM_SUCCESS;
    }
    COMM_BUS_RETURN_TYPE SPITeensy41Bus::read_register(
        unsigned char dev_addr, unsigned char reg_addr, unsigned char *reg_data, unsigned char cnt)
    {
        /* @to-do @eeymontt implement */
        return COMM_SUCCESS;
    }
    COMM_BUS_RETURN_TYPE SPITeensy41Bus::configureHardware()
    {
        /* @to-do @eeymontt implement */
        return COMM_SUCCESS;
    }

} // namespace eymonttdev