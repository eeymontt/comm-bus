#ifndef EYMDEV__SPI_COMM_BUS_TEENSY41__H
#define EYMDEV__SPI_COMM_BUS_TEENSY41__H

#include "spi_bus.h"

#include <Arduino.h>
#include <SPI.h>

namespace eymonttdev {

    /** Hardware-implemented SPI bus on Teensy 4.1. */
    class SPITeensy41Bus : public SPIBus
    {
        public:
            inline SPITeensy41Bus(cb_delay_fn fn_delay_ptr, SPIClass *spi, cb_cs_control_fn fn_cs_control_ptr)
                : SPIBus(fn_delay_ptr, fn_cs_control_ptr)
                , spi_(spi)
            {};
            virtual COMM_BUS_RETURN_TYPE write_register(
                unsigned char dev_addr, unsigned char reg_addr, const unsigned char *reg_data, unsigned char cnt) override;
            virtual COMM_BUS_RETURN_TYPE read_register(
                unsigned char dev_addr, unsigned char reg_addr, unsigned char *reg_data, unsigned char cnt) override;
            /** Configure hardware settings from within class itself, provided for convenience. */
            COMM_BUS_RETURN_TYPE configureHardware();
        private:
            SPIClass *spi_;
    };

} // namespace eymonttdev

#endif  /* EYMDEV__SPI_COMM_BUS_TEENSY41__H */

