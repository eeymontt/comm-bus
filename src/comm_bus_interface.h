#ifndef EYMDEV__COMM_BUS_INTERFACE__H
#define EYMDEV__COMM_BUS_INTERFACE__H

#include <stdint.h>

#define COMM_BUS_RETURN_TYPE    uint8_t
#define COMM_SUCCESS            COMM_BUS_RETURN_TYPE(0);
#define COMM_ERROR              COMM_BUS_RETURN_TYPE(1);
#define COMM_BUSY               COMM_BUS_RETURN_TYPE(2);
#define COMM_TIMEOUT            COMM_BUS_RETURN_TYPE(3);

namespace eymonttdev {

    typedef void (*cb_delay_fn)(uint16_t delay_ms);
    typedef void (*cb_receive_fn)(int num_bytes);
    typedef void (*cb_request_fn)(void);

    /** Communication bus interface for working with peripheral devices. */
    class PeripheralCommBus
    {
        public:
            inline PeripheralCommBus(cb_delay_fn fn_ptr) : delay_fn_(fn_ptr) {};
            /** Write to register as master. */
            virtual COMM_BUS_RETURN_TYPE write_register(
                unsigned char dev_addr, unsigned char reg_addr, const unsigned char *reg_data, unsigned char cnt) = 0;
            /** Read from register as master. */
            virtual COMM_BUS_RETURN_TYPE read_register(
                unsigned char dev_addr, unsigned char reg_addr, unsigned char *reg_data, unsigned char cnt) = 0;
            inline void delay(uint16_t dur_ms) { delay_fn_(dur_ms); }
            /** Assign ISR to execute when device receives a transmission, as slave. */
            inline COMM_BUS_RETURN_TYPE onReceive(cb_receive_fn fn_ptr)
            {
                user_onReceive_ = fn_ptr;
                return COMM_SUCCESS;
            }
            /** Assign ISR to execute when data is requested, as slave. */
            inline COMM_BUS_RETURN_TYPE onRequest(cb_request_fn fn_ptr)
            {
                user_onRequest_ = fn_ptr;
                return COMM_SUCCESS;
            }
        protected:
            cb_delay_fn delay_fn_;
            cb_receive_fn user_onReceive_{};
            cb_request_fn user_onRequest_{};
    };

} // namespace eymonttdev

#endif  /* EYMDEV__COMM_BUS_INTERFACE__H */ 