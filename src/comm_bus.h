#ifndef EYMDEV__COMM_BUS__H
#define EYMDEV__COMM_BUS__H

#if defined(__IMXRT1062__)
    #include "i2c_bus/i2c_bus_teensy41.h"
    #include "spi_bus/spi_bus_teensy41.h"
#endif

#endif  /* EYMDEV__COMM_BUS__H */