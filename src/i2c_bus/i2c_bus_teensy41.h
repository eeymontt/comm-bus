#ifndef EYMDEV__I2C_COMM_BUS_TEENSY41__H
#define EYMDEV__I2C_COMM_BUS_TEENSY41__H

#include "i2c_bus.h"

#include <Arduino.h>
#include <Wire.h>

namespace eymonttdev {

    /** Hardware-implemented I²C bus on Teensy 4.1. */
    class I2CTeensy41Bus : public I2CBus
    {
        public:
            inline I2CTeensy41Bus(cb_delay_fn fn_ptr, TwoWire *wire, int16_t addr=-1)
                : I2CBus(fn_ptr, addr)
                , wire_(wire)
            {};
            virtual COMM_BUS_RETURN_TYPE write_register(
                unsigned char dev_addr, unsigned char reg_addr, const unsigned char *reg_data, unsigned char cnt) override;
            virtual COMM_BUS_RETURN_TYPE read_register(
                unsigned char dev_addr, unsigned char reg_addr, unsigned char *reg_data, unsigned char cnt) override;
            /** Configure hardware settings from within class itself, provided for convenience. */
            COMM_BUS_RETURN_TYPE configureHardware();

            /** Write a byte as slave; provided to assist with development of ISR functions. */
            void write(const unsigned char data);
            /** Read a byte as slave; provided to assist with development of ISR functions. */
            unsigned char read();
        private:
            TwoWire *wire_;
    };

} // namespace eymonttdev

#endif  /* EYMDEV__I2C_COMM_BUS_TEENSY41__H */

