#include "i2c_bus_teensy41.h"

namespace eymonttdev {

    COMM_BUS_RETURN_TYPE I2CTeensy41Bus::write_register(
        unsigned char dev_addr, unsigned char reg_addr, const unsigned char *reg_data, unsigned char cnt)
    {
        wire_->beginTransmission(dev_addr);
        wire_->write(reg_addr);
        for(unsigned char index = 0; index < cnt; index++)
        {
            wire_->write(*reg_data);
            reg_data++;
        }
        wire_->endTransmission();
	    delay(150);
        return COMM_SUCCESS;
    }
    COMM_BUS_RETURN_TYPE I2CTeensy41Bus::read_register(
        unsigned char dev_addr, unsigned char reg_addr, unsigned char *reg_data, unsigned char cnt)
    {
        wire_->beginTransmission(dev_addr);
        wire_->write(reg_addr);
        wire_->endTransmission();
        delay(150);

        wire_->requestFrom(dev_addr, cnt);
        while(wire_->available())
        {
            *reg_data = wire_->read();
            reg_data++;
        }
        return COMM_SUCCESS;
    }
    COMM_BUS_RETURN_TYPE I2CTeensy41Bus::configureHardware()
    {
        if (slave_addr_ != -1)
        {
            // slave mode
            wire_->begin(slave_addr_);
        }
        else
        {
            // master mode
            wire_->begin();
            wire_->setClock(baud_rate_);    // if called, cannot be prior to begin()
        }
        
        if (user_onReceive_)
        {
            wire_->onReceive(user_onReceive_);
        }
        if (user_onRequest_)
        {
            wire_->onRequest(user_onRequest_);
        }

        /*
        @to-do @eeymontt
        add timeout functionality, not available natively via Teensyduino
        */

        return COMM_SUCCESS;
    }
    void I2CTeensy41Bus::write(const unsigned char data)
    {
        wire_->write(data);
    }
    unsigned char I2CTeensy41Bus::read()
    {
        auto data = (unsigned char)wire_->read();
        return data;
    }

} // namespace eymonttdev