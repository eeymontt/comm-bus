#ifndef EYMDEV__I2C_COMM_BUS__H
#define EYMDEV__I2C_COMM_BUS__H

#include "comm_bus_interface.h"

#define I2C_SPEED_STANDARD      100'000UL
#define I2C_SPEED_FAST          400'000UL
#define I2C_SPEED_HIGH_SPEED    3'400'000UL
#define I2C_SPEED_ULTRA_FAST    5'000'000UL

/* @url https://forum.pjrc.com/threads/57319-I2C-maximum-speed */
#define I2C_TEENSY_MAX          1'000'000UL

namespace eymonttdev {

    using GPIO_Pin = uint8_t;

    enum class I2C_Addr_t       { ADDR_7_BIT, ADDR_8_BIT, ADDR_10_BIT };
    enum class I2C_Byte_Order_t { MSBYTE, LSBYTE };
    
    struct I2C_Settings
    {
        const unsigned long speed;
        const I2C_Addr_t addr_bit_len;
        const I2C_Byte_Order_t byte_order;
        const GPIO_Pin SDA;
        const GPIO_Pin SCL;
    };

    class I2CBus : public PeripheralCommBus
    {
        public:
            inline I2CBus(cb_delay_fn fn_ptr, int16_t addr=-1)
                : PeripheralCommBus(fn_ptr)
                , addr_len_(I2C_Addr_t::ADDR_8_BIT)
                , byte_order_(I2C_Byte_Order_t::MSBYTE)
                , slave_addr_(addr)
            {};
            inline COMM_BUS_RETURN_TYPE init(I2C_Settings settings)
            {
                baud_rate_ = settings.speed;
                addr_len_ = settings.addr_bit_len;
                byte_order_ = settings.byte_order;
                SDA_ = settings.SDA;
                SCL_ = settings.SCL;
                return COMM_SUCCESS;
            }
            inline GPIO_Pin SDA() { return SDA_; }
            inline GPIO_Pin SCL() { return SCL_; }
            inline int16_t address() { return slave_addr_; }
            
        protected:
            unsigned long baud_rate_{I2C_SPEED_FAST};
            I2C_Addr_t addr_len_;
            I2C_Byte_Order_t byte_order_;
            int16_t slave_addr_;
            GPIO_Pin SDA_{255};
            GPIO_Pin SCL_{255};
    };

} // namespace eymonttdev

#endif  /* EYMDEV__I2C_COMM_BUS__H */